package com.ds;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


class MathHelperTest {

    @Test
    void sqrt() {
        Assert.assertEquals(MathHelper.sqrt(4), 2, 0);
        Assert.assertEquals(MathHelper.sqrt(430336), 656, 0);
        Assert.assertEquals(MathHelper.sqrt(4112), 64.1248781, 0.1);

        Assertions.assertThrows(ArithmeticException.class, () -> {
            double sqrt = MathHelper.sqrt(-4);
        });
    }
}