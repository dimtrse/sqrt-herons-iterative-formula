package com.ds;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input Operand: ");
        int operand = Integer.parseInt(scanner.nextLine());

        System.out.println("Result sqrt: " + MathHelper.sqrt(operand));
    }


}
