# Heron's Iterative Formula (SQRT)
App for operand calculation square root on Heron's Iterative Formula

## Heron's Iterative Formula

![Image of Heron's Iterative Formula](https://wikimedia.org/api/rest_v1/media/math/render/svg/9935d6f7061161b29325d712518fb58496f58bfb)

## Data

Input: 

 * Operand
 
 Output
 
  * Result sqrt
  
## Demo Screenshots


![Image of screen 1](./images/1.png)

## Testing app
![Image of screen 1](./images/test.png)