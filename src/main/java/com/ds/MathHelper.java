package com.ds;

public class MathHelper {

    /**
     * Square root calculation (Herons iterative formula)
     *
     * @param a - operand for calculation
     * @return square from operand `a`
     */
    public static double sqrt(long a) {
        if (a < 0)
            throw new ArithmeticException("Can not take sqrt of negative number");

        double b = a;
        int i = 0;
        while ((b * b > a) && (i < 200)) {
            b = (b + a / b) / 2;
            i++;
        }
        return b;
    }
}
